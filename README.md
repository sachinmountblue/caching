# Caching
-----------
## Introduction
-----

In today's world, software applications are dealing with a vast number of users and a massive amount of data, due to which software applications are facing some performance issue, like delay in content loading, delay in query processing.
To Overcome these performances and Scaling issues, it is essential for us to use Caching Technique.

### What is caching 
[**CACHING**](https://aws.amazon.com/caching)[1][^1] is a technique to store a subset of data to a temporary storage location called cache so that the future retrieval of cached data will faster than is possible by accessing the data’s primary storage location. Caching allow the efficient use of previously retrieved or computed data.

### Various types chaching
1. [Database caching](https://aws.amazon.com/caching/database-caching/#:~:text=A%20database%20cache%20supplements%20your,or%20as%20a%20standalone%20layer.)[2][^2]
1. [Indexing](https://www.microfocus.com/documentation/idol/IDOL_11_5/IDOLServer/Guides/html/English/expert/Content/IDOLExpert/IndexProcess/IndexCache.htm#:~:text=The%20index%20cache%20is%20a,quicker%20than%20writing%20to%20disk.)[3][^3]
1. [Content Delivery Network](https://www.cloudflare.com/en-gb/learning/cdn/what-is-a-cdn/)[4][^4]
1. [Server caching](https://blog.bitsrc.io/server-side-caching-in-expressjs-24038daec102)[5][^5]
1. [Browsers caching](https://pressidium.com/blog/2017/browser-cache-work/#:~:text=What%20is%20the%20browser%20cache,%2C%20Javascript%2C%20and%20so%20on.)[6][^6]

### Why we need caching
At the initial level, when the Software application has fewer concurrent client requests, everything works fine, but when concurrent client requests increase, we start facing performance issue.
1. Response time of Database increase.
1. Inconsistency in severe response time. 
1. CPU spikes when there is a large amount of concurrent request occurs.

We can resolve the above issue with the help of [**horizontal scaling**](https://www.section.io/blog/scaling-horizontally-vs-vertically/#:~:text=Horizontal%20scaling%20means%20scaling%20by,as%20%E2%80%9Cscaling%20up%E2%80%9D)[7][^7].In horizontal scaling, we increase our resource and machine power to handle the traffic. However, Horizontal scaling works to a certain level. after some time, we reach a point when the application can't handle client request irrespective of traffic. At this time, we can use caching technique to handle the client request.


### When caching techniques are useful

1. When clients want to retrieve the same data again and again.
1. When many clients request the same information.
1. Application that has read-heavy and write-heavy workload.

### When caching technique are not useful

1. If each time client request unique data that is not present in the cache, then our cache would have a negligible hit rate, and the cache does no good.
1. If cache data and source database are inconsistent, then caching techniques become less effective. Therefore we have to maintain our cache data and source database consistency.

## Some Strategies in caching
------

### Cache-Aside
------

In the cache-aside strategy,  the application/client can directly communicate with cache and database source.
The following steps happen in the cache-aside strategy.

1. First application/client request data from the cache, if data exist in the cache, then it is a [**cache hit**](https://www.cloudflare.com/en-in/learning/cdn/what-is-a-cache-hit-ratio/)[8][^8]. Now the application/client directly read the data from the cache and return it to the client.

1. If data is not present in the cache, then the application/client read it directly from the source database, and save a copy of the requested data in the cache memory, so that if we want to read the same data in future, then we can directly read it from the cache.

#### Use-case
It's, mainly used in read-heavy workloads. (newspaper website, blogs).

#### Advantages
1. Very Easy to implement.
1. Even after a [**cache-miss**](https://hazelcast.com/glossary/cache-miss/)[9][^9], the application works fine, but the performance degrades. Now it has to fetch data directly from the database, which is time-consuming.
1. It avoids filling of non requested data.

 
#### Disadvantages
1. The cache-aside strategy is not great if your data source is changing rapidly and getting updated from multiple sources because it increases the inconsistency between cache and database. Inconsistency increases the cache-miss. Due to cache-miss, you have to fetch data from the database, and you may encounter a noticeable delay in your application.




### Read through cache
-------
In the "read-through-cache," strategy the Application/client can not directly access the database. First, it checks the data in The Cache. If the cache miss occurs, then. First, The Cache will populate with the missing data from the database after that cache returns the requested data to the client. 
In both strategies, "Read-through-cache" and "cache-aside". Data is loaded lazily. e.g., data will get copied in the cache. Only if, It is requested by the application/client. 

#### Use-case 
It is Mostly, Similar to the "cache-aside", and it is also mainly used in read-heavy applications. The only difference between the two is that it always sends the result through cache.

#### Advantages
It is Mostly suitable for read-heavy workload, the applications where the same type of data is requested many times. e.g. blogs website, a news website.

#### Disadvantages
When the data is requested the first time, we encounter a noticeable delay due to a cache-miss, e.g. data is not present in the cache.


 
### Write through cache
-----
In the "write-through-cache "strategy, we First perform a write operation on the cache, and when the "cache" gets updated, "cache" will update the Main database with newly received data. In this strategy, "cache" and "database" remain synchronized. Because both reading and writing operations us performed by the "cache".

 The following steps are performed in the "write-through-cache" strategy.

1. Application/client interact with cache and write some data into the "cache".
1. cache simultaneously write data into the database
1. Cache return the requested data to the client

 
####Advantages
It maintains the data consistency between "cache" and "database".

####Disadvantages
This strategy slows down the overall operation and performance. Because, In this Strategy, we first write the data into the "cache"  then cache writes data into the database, which slow down the overall speed.  But, if we retrieve the same data in future, then it will respond very fast.

## Conclusion
----
Here I discussed only a few strategies because caching is a  broad topic. It contains an enormous amount of techniques according to our requirements.
Before selecting, Any caching strategy or the combination of Strategies. We must be sure about our goals, requirements and read-write pattern of our application.
 If we choose the wrong caching technique, it may slow down our application or We may not use the caching technique to their fullest potential.





## References
[^1]: https://aws.amazon.com/caching
[^2]: https://blog.sqlauthority.com/2019/08/24/sql-server-identify-read-heavy-workload-or-write-heavy-workload-type-by-counters/
[^3]: https://www.microfocus.com/documentation/idol/IDOL_11_5/IDOLServer/Guides/html/English/expert/Content/IDOLExpert/IndexProcess/IndexCache
[^4]: https://www.cloudflare.com/en-gb/learning/cdn/what-is-a-cdn/
[^5]: https://blog.bitsrc.io/server-side-caching-in-expressjs-24038daec102
[^6]: https://pressidium.com/blog/2017/browser-cache-work/
[^7]: https://www.section.io/blog/scaling-horizontally-vs-vertically/
[^8]: https://www.cloudflare.com/en-in/learning/cdn/what-is-a-cache-hit-ratio/
[^9]: https://hazelcast.com/glossary/cache-miss/
[^10]:https://medium.com/system-design-blog/what-is-caching-1492abb92143#:~:text=It%20is%20mostly%20used%20with%20an%20application%20with%20read%2Dheavy%20workloads.&text=If%20data%20is%20found%20in,then%20it's%20a%20cache%2Dmiss.
[^11]: https://blog.logrocket.com/caching-strategies-to-speed-up-your-api/
[^12]: https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/
[^13]: http://tutorials.jenkov.com/software-architecture/caching-techniques.html


































